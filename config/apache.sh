<VirtualHost www.feapi.com:80>
        ServerName www.feapi.com
        ServerAdmin webmaster@localhost

		DocumentRoot /var/www/html/feproxy

        ErrorLog /var/www/html/feproxy/proxyfeerror.log
        CustomLog /var/www/html/feproxy/proxyfeaccess.log combined
</VirtualHost>

<VirtualHost www.ctrbpi.com:80>
        ServerName www.ctrbpi.com
        ServerAdmin webmaster@localhost
		
		DocumentRoot /var/www/html/ctrbpi
        
        ErrorLog /var/www/html/ctrbpi/error.log
        CustomLog /var/www/html/ctrbpi/access.log combined
</VirtualHost>

<VirtualHost www.themwa.com:80>
        ServerName www.themwa.com
        ServerAdmin webmaster@localhost
		
		DocumentRoot /var/www/html/themwa
        
        ErrorLog /var/www/html/themwa/error.log
        CustomLog /var/www/html/themwa/access.log combined
</VirtualHost>


sudo mkdir /var/www/html/themwa
sudo mkdir /var/www/html/ctrbpi
sudo mkdir /var/www/html/feproxy

sudo touch /var/www/html/themwa/index.html
sudo touch /var/www/html/ctrbpi/index.html
sudo touch /var/www/html/feproxy/index.html

sudo chown ct /var/www/html/themwa/index.html
sudo chown ct /var/www/html/ctrbpi/index.html
sudo chown ct /var/www/html/feproxy/index.html

chmod 755 /var/www/html/themwa/index.html
chmod 755 /var/www/html/ctrbpi/index.html
chmod 755 /var/www/html/feproxy/index.html

echo "Hello on themwa page" > /var/www/html/themwa/index.html
echo "Hello on ctrbpi page" > /var/www/html/ctrbpi/index.html
echo "Hello on feproxy page" > /var/www/html/feproxy/index.html