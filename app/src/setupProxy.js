const proxy = require('http-proxy-middleware');
module.exports = function(app) {
    app.use(
        '/api',
        proxy({ target: 'http://82.229.120.121', changeOrigin: true })
      );
    app.use(
        '/rapi',
        proxy({ target: 'http://82.229.120.121', changeOrigin: true })
      );
};
