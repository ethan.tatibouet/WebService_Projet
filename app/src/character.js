import React from 'react';

class Character extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            PV: 15,
            Atk: 7,
            Def: 10,
            Movement: 3,
            Range: 4
        }
    }
}

export default Character