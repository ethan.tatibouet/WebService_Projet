import React from 'react';
import './board.css';
import {getData} from './request';
const R = require('ramda');

class Square extends React.Component {
    constructor(props){ super(props) }
    squareEmpty(){
        return (
            <button onClick={this.props.customClickEvent} className={'square'}
                style={{backgroundImage: `url("data:image/jpeg;base64,${this.props.image}")`}} >
            </button>
        )
    }
    render() { return (this.squareEmpty()) }
}

class Board extends React.Component {
    constructor(props){
        super(props);
        this.handleClick = this.handleClick.bind(this)
        this.state = { images: {} }
    }
    componentDidMount(){ 
        this.getImgs()
    }
    handleClick(index, subIndex){
        if(!this.props.board[index][subIndex].includes('player'))
            this.props.lose()
        this.setState({board:this.props.board.slice()[index][subIndex] = ''})
        this.props.isWin()
    }

    getImgs = () =>{
        /*let res = await getData('/rapi/images?field=water,wall,path,forest')
        this.setState({ images: res })*/

        new Promise( (resolve) => {
            resolve(getData(`/rapi/images?field=water,wall,path,forest`));
        }).then((response) => {
            this.setState({images: response})            
            new Promise( (resolve) => { resolve(getData(`/rapi/player/1`)) }).
            then((response) => {
                let newImage = {...this.state.images}
                newImage.player1 = response
                this.setState({images: newImage})
            } ).catch(error => console.log(error))

            new Promise( (resolve) => { resolve(getData(`/rapi/player/2`)) }).
            then((response) => {
                let newImage = {...this.state.images}
                newImage.player2 = response
                this.setState({images: newImage})
            } ).catch(error => console.log(error))

            new Promise( (resolve) => { resolve(getData(`/rapi/player/3`)) }).
            then((response) => {
                let newImage = {...this.state.images}
                newImage.player3 = response
                this.setState({images: newImage})
            } ).catch(error => console.log(error))
        }).catch(error => console.log(error))
        
    }
    
    render() {
        return (
            this.props.board.map( 
                (element,index) => ( <> 
                    <div className="board-row"> 
                        {element.map( (subElement,subIndex) => 
                        <Square key={index+'/'+subIndex}  customClickEvent={ () => this.handleClick(index,subIndex) } 
                        className={subElement} image={ this.state.images[subElement] } /> )}
                    </div>        
                </> )
                )
            )
    }
}

class Param extends React.Component {
    constructor(props){ 
        super(props)
        this.state = {
            dbconnection: '',
        }
        this.db_status = this.db_status.bind(this)
     }
    componentDidMount(){
        new Promise( (resolve) => { resolve(getData(`/api/server3.py`)) }).
            then((response) => {this.setState({dbconnection: response})
            } ).catch(error => console.log(error))        
    }

    db_status(){
        if(this.state.dbconnection.includes('MariaDB'))
        return <span>Connexion base de données ✔</span>
        return <span>Connexion base de données ❌</span>
    }

    render(){
        return (<>
            <label>Change de niveau : </label>
            <span onClick={() => this.props.changeLevel(-1)}>➖</span>
            <span>{this.props.levelId}</span>
            <span onClick={() => this.props.changeLevel(+1)}>➕</span>
            <br/>
            {this.db_status()}       
        </>)
    }
}


class WinPopup extends React.Component {
    constructor(props){ super(props) }
    render(){
        if(this.props.isWin == 'win')
            return <h1>Bravo tu as fini le jeu</h1>
        if(this.props.isWin == 'lose')
            return <h1>Tu as perdu, essai encore</h1>
        return <h1>A toi de jouer</h1>
    }
}

class Game extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
            isLoaded:false,
            levelName:'',
            levelId:1,
            board:Array,
            life:10
      }
      this.display = this.display.bind(this)
      this.loseLife = this.loseLife.bind(this)
      this.handleClick = this.handleClick.bind(this)
    }
    componentDidMount(){ this.fillMap(this.state.levelId) }

    loseLife(){
        if(this.state.life>0)
            this.setState({life:this.state.life-1})
        if(this.state.life-1==0)
            this.setState({isWin:'lose'})
    }
    fillMap = async (id) => {
        if(this.state.levelId >= 3)
            return this.setState({isWin:'win'})
        if(this.state.isWin=='lose')
            return
        let res = await getData('/api/server4.py?id='+id)
        this.setState({
            isLoaded: true,
            levelName: res.name,
            levelId: id,
            board: res.plateau
        })
    }
    handleClick() {
        let playersAlive = false
        let doNothing
        this.state.board.map( (element,index) => element.map((subElement) => subElement.includes('player') ? playersAlive = true : doNothing = 'none'))
        if(!playersAlive){
            this.fillMap(this.state.levelId+1)
        }
    }
    display(){
        if(this.state.isLoaded)
            return (<>
                <h1>Level {this.state.levelName}</h1>
                <label>Il te reste {this.state.life} vie(s)</label>
                <p>{Array(this.state.life).fill().map(() => <span> ♥ </span>)}</p>
                <Board {...this.state} lose = { () => this.loseLife()} isWin = { () => this.handleClick()}/>
                <Param {...this.state} changeLevel = { (value) => this.fillMap(this.state.levelId+value)}/>
                <WinPopup isWin={this.state.isWin}/>                                
            </>)
        return <div>Loading...</div>
    }

    render() {
        return (
          <div className="game">
            <div className="game-board">
                {this.display()}
            </div>
          </div>
        );
      }
}

function colorRange(CharacterRange, CharacterPosition) {
    //board[CharacterPosition[0], CharacterPosition[1]] = <Square key={0} className='range' />
    // console.log("color range");
}

function handleKeyPress(event){
    colorRange(2, [5,5]);
    // console.log(event.key)
}
document.body.addEventListener('keydown', handleKeyPress, false)

export default Game;
