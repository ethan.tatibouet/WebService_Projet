import axios from 'axios';

/**
 * 
 * @param {String} url 
 * @return {JSON}
 */
export async function getData(url){
    return await axios.get(url)
        .then(response =>{
            return response.data;
        });
  }

// class Request extends React.Component {
//     constructor(props) {
//       super(props);
//       this.state = {
//         error: null,
//         isLoaded: false,
//         items: {}
//         };
//     }
//     getData = async () => {

//         let res = await axios.get('/api/server4.py');
//         this.setState({
//             isLoaded: true,
//             items: res.data
//         });
//         return await res.data;
//         // axios.get(`https://opentdb.com/api.php?amount=10`)
//         // axios.get('/api/server4.py',{
//         //             method: 'GET',
//         //             // headers: {
//         //             //     'Access-Control-Allow-Origin': '*',
//         //             //     'Content-Type': 'application/json'
//         //             // }
//         //             // crossDomain: true,
//         //             // changeOrigin: true
//         //         })
//         //     .then(res => {
//         //         this.setState({
//         //             isLoaded: true,
//         //             items: res.data
//         //         });
//         //         console.log('Loading complete')
//         //     })
//         //     .catch(error => {
//         //         console.log(error)
//         //     });
//         // fetch("https://opentdb.com/api.php?amount=10", { method: 'GET', headers: { "access-control-allow-origin" : "*", "Content-type": "application/json; charset=UTF-8" } })
//         // .then(res => res.json())
//         // .then(
//         //     (result) => {
//         //         this.setState({
//         //             isLoaded: true,
//         //             items: result.items
//         //         });
//         //         console.log("Request réussie");
//         //         // console.log(this.state.items);
//         //     },
//         //     // Remarque : il est important de traiter les erreurs ici
//         //     // au lieu d'utiliser un bloc catch(), pour ne pas passer à la trappe
//         //     // des exceptions provenant de réels bugs du composant.
//         //     (error) => {
//         //         this.setState({
//         //             isLoaded: true,
//         //             error
//         //         });
//         //         console.log('Erreur');
//         //         console.log(error);
//         //     }
//         // )
//     }
//     componentDidMount() {
//         this.getData();
//     }
//     render() {
//         console.log("Render Request.js")
//         console.log(this.state.items)
//         if (!this.state.isLoaded) {
//             return <div>Loading...</div>
//         }
//         else {
//             return(
//             <div>
//                 Loading complete
//             </div>
//             )
//             // return this.state.items;
//             // return(
//             //     <div>
//             //         Loading complete
//             //         <ul>
//             //             <li>Nom: {this.state.items.name}</li>
//             //             <li>
//             //             {this.state.items.plateau.map( item => 
//             //                 <ul>
//             //                 {item.map(other => 
//             //                     <li>{other}</li>
//             //                 )}
//             //                 </ul>
//             //             )}
//             //             </li>
//             //             {/* {items.results.map((category, difficulty) => (
//             //                 <li className="travelcompany-input" key={category}>
//             //                     <span className="input-label">key: {category} Name: {difficulty}</span>
//             //                 </li>
//             //             ))} */}
//             //         </ul>
//             //     </div>
//             // )
//         }
//     }
// }
