# Projet Fire Embleme (FE)

## Cours

https://slides.com/benjamintourman/rest#/2

https://slides.com/benjamintourman/webservice#/

## Consignes

* 2 micro-services
* micro-serveur
* languages libres
* tests
* doc api (swagger)
* alerting applicatif

## Map

## CGI

### APACHE2

```bash
sudo service apache2 stop 
sudo apt-get purge apache2 apache2-utils apache2.2-bin apache2-common
sudo a2enmod headers
sudo a2enmod rewrite

sudo apt-get autoremove
whereis apache2 
sudo rm -rf /etc/apache2

sudo apt install apache2
sudo a2enmod cgi
sudo systemctl restart apache2
sudo a2ensite exemple.com
sudo a2dissite exemple.com
sudo systemctl reload apache2   

sudo a2enconf  
sudo a2disconf 

sudo a2enmod 
sudo a2dismod 

apachectl configtest
apachectl -t
```

```bash
sudo a2enmod proxy
sudo a2enmod proxy_http
sudo service apache2 restart

<VirtualHost *:80>
    ServerName cups.domaine.fr
    ServerAdmin postmaster@domaine.fr
 
	ProxyPass "/ruby" "http://127.0.0.1:4567/"
	ProxyPassReverse "/ruby" "http://127.0.0.1:4567/"
    ProxyRequests Off
</VirtualHost>

# a2ensite cups.domaine.fr.conf
sudo service apache2 reload
```

> https://www.linux.com/tutorials/configuring-apache2-run-python-scripts/
```xml
#Header set Access-Control-Allow-Origin "*"

# Always set these headers.
Header always set Access-Control-Allow-Origin "*"
Header always set Access-Control-Allow-Methods "POST, GET, OPTIONS, DELETE, PUT"
Header always set Access-Control-Max-Age "1000"
Header always set Access-Control-Allow-Headers "x-requested-with, Content-Type, origin, authorization, accept, client-security-token"
#Header always set Access-Control-Allow-Headers "Cache-Control, If-Modified-Since, X-Requested-With, Content-Type, Origin, Authorization, Accept, Client-Security-Token, Accept-Encoding, Pragma"
Header always set Access-Control-Allow-Credentials "true"
     
# Added a rewrite to respond with a 200 SUCCESS on every OPTIONS request.
RewriteEngine On
RewriteCond %{REQUEST_METHOD} OPTIONS
RewriteRule ^(.*)$ $1 [R=200,L]

AllowOverride All

<Directory />
	Options +FollowSymLinks
	AllowOverride None
	Require all denied
</Directory>

<Directory /var/>
	Require all granted
</Directory>

ScriptAlias /test/ /var/feweb/script/
#Alias /cgi-bin /var/www/cgi-bin
<Directory /var/feweb/>
        Order allow,deny
        Allow from all
        Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch +FollowSymLinks
        AllowOverride None
        
        SetHandler cgi-script
        Require all granted
        AddHandler cgi-script .py
</Directory>


<Directory /var/feweb/script/>
        Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch +Indexes
        AddHandler cgi-script .py
        AllowOverride None
        Require all granted
</Directory>
```

```xml
<IfModule mod_alias.c>
	<IfModule mod_cgi.c>
		Define ENABLE_USR_LIB_CGI_BIN
	</IfModule>

	<IfModule mod_cgid.c>
		Define ENABLE_USR_LIB_CGI_BIN
	</IfModule>


	<IfDefine ENABLE_USR_LIB_CGI_BIN>
		ScriptAlias /test/ /var/feweb/script/
		
		<Directory "/var/feweb/script/">
			AllowOverride None
			Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
			Require all granted
		</Directory>
		
		<Directory "/var/feweb/script/">
			AllowOverride None
			Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
			Require all granted
		</Directory>
	</IfDefine>

</IfModule>
```

### PYTHON3

```bash
sudo apt install python3
sudo apt install python3-pip
sudo apt install python3-pymysql
pip3 install pymysql
#pip3 install mysqlclient
#pip3 install configParser
#pip3 install mysql
```

```python
#!/usr/bin/python3
# -*- coding: UTF-8 -*-# enable debugging
import cgitb
cgitb.enable()
print('Content-Type: text/html;charset=utf-8')
print()
print('Hello World!')
```

## Ruby

```bash
sudo apt-get install ruby-full
sudo gem install sinatra
sudo gem install json
```

## DataBase

```bash
sudo apt-get install mariadb-server

sudo service mysql start
sudo service mysql restart
sudo service mysql stop

sudo mysql_secure_installation

mysql -u root
#mysql -u root -p
create database fe;

#ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'le_mot_de_pass_choisi';
CREATE USER 'admin'@'localhost' IDENTIFIED BY 'patate';

GRANT USAGE ON *.* TO 'admin'@'localhost' IDENTIFIED BY 'patate';
GRANT ALL ON *.* TO 'admin'@'localhost' IDENTIFIED BY 'patate';
FLUSH PRIVILEGES;
# Enable remote access
#GRANT USAGE ON *.* TO 'myuser'@'%' IDENTIFIED BY 'mypassword';

sudo mysql
mysql -u admin -p fe 
```

## SSH

ssh ct@82.229.120.121 -p 443

http://www.feapi.com/api/gitpull.sh
http://www.feapi.com/api/server2.py?funcName=Non%20de%20la%20fonction



## Liens

https://perhonen.fr/blog/2015/05/un-reverse-proxy-apache-avec-mod_proxy-1713
http://httpd.apache.org/docs/2.4/fr/mod/mod_proxy.html#access