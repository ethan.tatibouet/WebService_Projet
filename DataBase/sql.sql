-- create database fe;
-- use fe;
-- show databases;
-- show tables;

-- === Table des statistiques des personnages ===
drop table personnage;
create table personnage(
	id int not null auto_increment PRIMARY KEY,
	nom varchar(255) null,
	niveau int default 1,
	pv int default 1,
	atk int default 0,
	def int default 0,
	move int default 0,
	rangeAtk int default 0
);
-- Personnages principaux
insert into personnage(nom, pv, atk, def, move, rangeAtk) values('Marth',18,5,7,5,1);
insert into personnage(nom, pv, atk, def, move, rangeAtk) values('Felix',25,3,8,4,1);
insert into personnage(nom, pv, atk, def, move, rangeAtk) values('Sylvain',16,4,4,6,1);
insert into personnage(nom, pv, atk, def, move, rangeAtk) values('Dimitri',28,5,7,3,1);
insert into personnage(nom, pv, atk, def, move, rangeAtk) values('Edelgard',25,5,6,5,2);
insert into personnage(nom, pv, atk, def, move, rangeAtk) values('Claude',23,4,6,4,3);
-- Personnages PNJ
insert into personnage(nom, pv, atk, def, move, rangeAtk) values('Archer',18,4,3,5,3);
insert into personnage(nom, pv, atk, def, move, rangeAtk) values('Chevalier',22,3,4,4,1);
-- select * from personnage;

-- === Table des cartes disponibles ===
-- VARCHAR(8192) or JSON
drop table carte;
create table carte(
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(32) AS (JSON_VALUE(plateau, '$.name')),
	plateau JSON,
	carteField VARCHAR(32) AS (JSON_VALUE(plateau, '$.zone')),
	CHECK (plateau IS NULL OR JSON_VALID(plateau))
);

delete from carte;
CREATE INDEX carte_zone ON carte(carteField);
insert into carte(plateau) values('{ "name":"carte1", "zone":"foret", "plateau":[ ["wall","wall","wall","wall","wall","wall","wall","wall","forest"], ["wall","forest","forest","forest","forest","forest","forest","wall","forest"], ["wall","path","path","path","path","path","path","wall","forest"], ["wall","path","path","path","path","path","path","wall","forest"], ["wall","path","path","path","path","path","path","wall","forest"], ["wall","path","path","path","path","path","path","wall","forest"], ["wall","path","path","path","path","path","path","wall","forest"], ["wall","path","path","path","path","path","player2","wall","forest"], ["wall","path","path","path","path","path","path","wall","forest"], ["wall","path","path","path","player3","path","path","wall","forest"], ["wall","path","path","path","path","path","path","wall","forest"], ["wall","path","path","player1","path","path","path","wall","forest"], ["wall","wall","wall","wall","wall","wall","wall","wall","forest"], ["forest","water","player2","water","water","water","water","forest","forest"] ] }' );
insert into carte(plateau) values('{ "name":"carte2", "zone":"plaine", "plateau":[ ["wall","wall","wall","water","water","water","wall","wall","forest"], ["wall","forest","forest","water","water","forest","forest","wall","forest"], ["wall","path","path","water","water","path","player1","wall","forest"], ["wall","path","path","water","water","path","path","wall","forest"], ["wall","path","path","water","water","path","path","wall","forest"], ["wall","path","path","water","water","path","path","wall","forest"], ["wall","path","path","water","water","path","path","wall","forest"], ["wall","path","path","water","water","path","path","player3","forest"], ["wall","path","path","water","water","path","path","wall","forest"], ["wall","player2","path","water","water","path","path","wall","forest"], ["wall","path","path","water","water","path","path","wall","forest"], ["wall","player2","path","water","water","path","path","wall","forest"], ["wall","wall","wall","water","water","wall","wall","wall","forest"], ["forest","water","water","water","water","water","water","forest","forest"] ] }' );
insert into carte(plateau) values('{ "name":"carte3", "zone":"plaine", "plateau":[ 

["wall","wall","wall","water","water","water","wall","wall","forest","wall","path","path","water","water","path","path","wall","forest"], ["wall","wall","wall","water","water","wall","wall","wall","forest","wall","player1","path","water","water","path","path","wall","forest"],
["forest","water","water","water","water","water","water","forest","forest","wall","player1","path","water","water","path","path","wall","forest"],
["wall","forest","forest","water","water","forest","forest","wall","forest","wall","path","path","water","water","path","path","wall","forest"], ["wall","path","path","water","water","path","player3","wall","forest","wall","path","path","water","water","path","path","wall","forest"], ["wall","path","path","water","water","path","path","wall","forest","wall","path","path","water","water","path","path","wall","forest"], ["wall","path","path","water","water","path","path","wall","forest","wall","path","path","water","water","path","path","wall","forest"], ["wall","path","path","water","water","path","path","wall","forest","wall","path","path","water","water","path","path","wall","forest"], ["wall","path","path","water","water","path","path","wall","forest","wall","path","path","water","water","path","path","wall","forest"], ["wall","path","path","water","water","path","path","player3","forest","wall","path","path","water","water","path","path","wall","forest"], ["wall","path","path","water","water","path","path","wall","forest","wall","path","path","water","water","path","path","wall","forest"], ["wall","player2","path","water","water","path","path","wall","forest","wall","path","path","water","water","path","path","wall","forest"], ["wall","path","path","water","water","path","path","wall","forest","wall","path","path","water","water","path","path","wall","forest"], ["wall","player2","path","water","water","path","path","wall","forest","wall","player3","path","water","water","path","path","wall","forest"],  ["wall","player2","path","water","water","path","path","wall","forest","wall","path","path","water","water","path","path","wall","forest"] 


] }' );
insert into carte(plateau) values('{ "name":"carte4", "zone":"plaine" }' );
-- select name, carteField from carte;
-- select plateau from carte;

-- === Table des parties en cours ===
drop table game;
create table game(
	id int not null auto_increment PRIMARY KEY,
	idJoueur1 JSON,
	idJoueur2 JSON,
	carte JSON,
	CHECK (idJoueur1 IS NULL OR JSON_VALID(idJoueur1)),
	CHECK (idJoueur2 IS NULL OR JSON_VALID(idJoueur2)),
	CHECK (carte IS NULL OR JSON_VALID(carte))
);
-- desc game;