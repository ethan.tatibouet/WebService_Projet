<?php

class Config
{
    const SERVERNAME = "localhost";
    const DBNAME = "tutoratphpew1";
    const USER = "root";
    const PASSWORD = "";
}

$GLOBALS['db'] = new PDO("mysql:host=" . Config::SERVERNAME . ";dbname=" . Config::DBNAME, Config::USER, Config::PASSWORD);

function selectAllFromEtudiant()
{
    $all = $GLOBALS['db']->prepare('SELECT * FROM etudiant');
    $all->execute();
    $all = $all->fetchAll();
    $all = json_encode($all);
    return $all;
}


echo selectAllFromEtudiant();
