#!/usr/bin/ruby
# frozen_string_literal: true

require 'airborne'

describe 'sample spec' do
  it 'should validate types' do
    get 'http://127.0.0.1:4567/images?field=water,wall,path'
    expect_json_types(name: :string)
  end

  it 'should validate values' do
    get 'http://127.0.0.1:4567/images?field=water,wal,path'
    expect_json(name: :string)
  end
end
