#!/usr/bin/ruby
# frozen_string_literal: true

require 'net/http'
require 'open-uri'

def call(url)

  uri = URI(url)

  Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == 'https') do |http|
    request = Net::HTTP::Get.new uri
    response = http.request request
    response.body
  end
end
