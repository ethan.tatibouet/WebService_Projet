#!/usr/bin/ruby
# frozen_string_literal: true

require 'base64'

Path = File.expand_path File.dirname(__FILE__).to_s

def get_player(par)
  Dir.foreach("#{Path}/sprites/players") do |f|
    if File.file?("#{Path}/sprites/players/#{f}")
      File.open("#{Path}/sprites/players/player#{par}.png", 'rb') do |i|
        $image = Base64.strict_encode64(i.read)
      end
    end
  end
  $image
end

def p_exists(file)
  bla = ''
  Dir.foreach("#{Dir.pwd}/sprites/players") do |f|
    if f.to_s == "player#{file}.png"
      bla = 1
      break
    else
      bla = 0
    end
  end
  bla
end