#!/usr/bin/ruby
# frozen_string_literal: true

require 'base64'
require 'json'

Path = File.expand_path File.dirname(__FILE__).to_s

def get_field(par)
  r = ''
  Dir.foreach("#{Path}/sprites/#{par}") do |f|
    if File.file?("#{Path}/sprites/#{par}/#{f}")
      File.open("#{Path}/sprites/#{par}/#{f}", 'rb') do |i|
        r = Base64.strict_encode64(i.read)
      end
    end
  end
  r
end

def get_fields(fields)
  @hash = {}
  fields.split(',').each do |v|
    @hash[v] = if f_exists(v) == 1
                 get_field(v)
               else
                 "Wrong param : #{v}"
               end
  end
  @hash.to_json
end

def f_exists(dirname)
  bla = ''
  Dir.foreach("#{Dir.pwd}/sprites") do |d|
    if d.to_s == dirname
      bla = 1
      break
    else
      bla = 0
    end
  end
  bla
end
