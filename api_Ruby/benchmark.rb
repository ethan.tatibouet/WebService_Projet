#!/usr/bin/ruby
# frozen_string_literal: true

require 'benchmark'
include Benchmark
load "#{File.expand_path File.dirname(__FILE__)}/call.rb"

ARR = %w[water path forest wall].freeze

def random_params
  r = rand(1..4)
  f_array = []
  r.times do |i|
    f_array.push(ARR[rand(3)])
  end
  f_array.map { |i| i }.join(',')
end

def benchmark_image
  i = 1_000
  Benchmark.benchmark(CAPTION, 7, FORMAT, '>total:', '>avg:') do |x|
    tt = x.report('times:') do
      i.times do
        call("http://82.229.120.121/rapi/images?field=#{random_params}") do |k, v|
          [k.to_sym, v].to_h
        end
      end
    end
    te = x.report('each:') do
      (1..i).to_a.each do
        call("http://82.229.120.121/rapi/images?field=#{random_params}") do |k, v|
          [k.to_sym, v].to_h
        end
      end
    end
    tu = x.report('upto:') do
      1.upto(i) do
        call("http://82.229.120.121/rapi/images?field=#{random_params}") do |k, v|
          [k.to_sym, v].to_h
        end
      end
    end
    [tt + te + tu, (tt + te + tu) / 3]
  end
end

