#!/usr/bin/ruby
# frozen_string_literal: true

require 'sinatra'
require 'json'
require 'discordrb'
load "#{File.expand_path File.dirname(__FILE__)}/field.rb"
load "#{File.expand_path File.dirname(__FILE__)}/player.rb"
load "#{File.expand_path File.dirname(__FILE__)}/benchmark.rb"

@bot = Discordrb::Commands::CommandBot.new token: 'NjUxMzgyNjg1OTM5NDAwNzI0.Xi6avg.I9SKOHbwp-GuQ2GqmVLsF4tJgFE', client_id: 651382685939400724, prefix: "~"

Thb = Thread.new do
  load "#{File.expand_path File.dirname(__FILE__)}/bot.rb"
end
Thb.join
Sinatra::Application.environment == :production

GLOBAL = 'http://82.229.120.121/rapi'

# benchmark_image
get '/image/:field' do |param|
  if f_exists(param.to_s) == 1
    get_field(param)
  else
    status 400
    body 'Wrong param.'
    alerting("#{GLOBAL}#{request.path_info}", 400)
  end
end

get '/images' do
  @param = params[:field]
  # C'est giga sale, mais ça marche BISOUS
  if get_fields(@param)[1] != '}'
    get_fields(@param)
  else
    status 400
    body 'Wrong param.'
    param = "?field=#{@param}"
    alerting("#{GLOBAL}#{request.path_info}#{param}", 400)
  end
end

get '/player/:id' do |param|
  if p_exists(param.to_s) == 1
    get_player(param)
  else
    status 400
    body 'Wrong param.'
    alerting("#{GLOBAL}#{request.path_info}", 400)
  end
end
