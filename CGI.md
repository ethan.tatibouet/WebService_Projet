# Setup CGI with Apache2

```bash
sudo apt-get install apache2
sudo apt-get install curl
curl http://127.0.0.1/
```

> sudo nano /etc/apache2/conf-available/serve-cgi-bin.conf
```xml
<IfModule mod_alias.c>
	<IfModule mod_cgi.c>
		Define ENABLE_USR_LIB_CGI_BIN
	</IfModule>
 
	<IfModule mod_cgid.c>
		Define ENABLE_USR_LIB_CGI_BIN
	</IfModule>
 
	<IfDefine ENABLE_USR_LIB_CGI_BIN>
		ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
		ScriptAlias /api/ /usr/lib/cgi-bin/
		<Directory "/usr/lib/cgi-bin">
			AllowOverride None
			Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
			Require all granted
		</Directory>
	</IfDefine>
</IfModule>
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
```

```bash
cd /etc/apache2/mods-enabled
sudo ln -s ../mods-available/cgi.load
sudo service apache2 reload
```

> sudo nano /usr/lib/cgi-bin/hw.sh
```bash
#!/bin/bash
printf "Content-type: text/html\n\n"
printf "Hello World!\n"
```

```bash
sudo chmod +x /usr/lib/cgi-bin/hw.sh
curl http://127.0.0.1/cgi-bin/hw.sh
#dos2unix hw.sh
#Hello World!
```

