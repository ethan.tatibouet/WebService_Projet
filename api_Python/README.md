# README python

* Update le repo git sur le raspberry
	* http://www.feapi.com/api/gitpull.sh
* Tester la connection base de données
	* http://www.feapi.com/api/server3.py
* Recupere un plateau
	* http://www.feapi.com/api/server4.py

## Scripts

### Git Pull
```shell
#!/bin/bash
cd /var/fe/WebService_Projet
OUTPUT="$(git pull)"
printf "Content-type: text/html\n\n"
echo "Git updating..."
echo "${OUTPUT}"
```

```python
#!/usr/bin/python3
# -*- coding: UTF-8 -*-# enable debugging
import cgi, cgitb
print("Content-type: text/html\n\n")
requestData = cgi.FieldStorage()
funcName = requestData.getvalue('funcName')
print (funcName)
```
https://www.tutorialspoint.com/python3/python_database_access.htm

```python
#!/usr/bin/python3
# -*- coding: UTF-8 -*-# enable debugging
import pymysql
import json
import cgi, cgitb

print ('Content-Type: application/json')
print ('\r\n\r\n')

requestData = cgi.FieldStorage()
id = requestData.getvalue('id')

db = pymysql.connect("localhost","admin","patate","fe" )
cursor = db.cursor()
if id == None:
	id = 1
cursor.execute("select plateau from carte where id="+str(id))
for row in cursor:
	print(row[0])
#data = cursor.fetchone()
db.commit()
db.close()
#print (json.dumps(data))
```

```python
#!/usr/bin/python3
# -*- coding: UTF-8 -*-# enable debugging
import pymysql
import cgi

print ('Content-Type: application/json')
print ('\r\n\r\n')

db = pymysql.connect("localhost","admin","patate","fe" )
cursor = db.cursor()
cursor.execute("select version()")
data = cursor.fetchone()
db.commit()
db.close()
print (json.dumps(data))
```

```python
#!/usr/bin/python3
import json

print ('Content-Type: application/json')
print ('\r\n\r\n')
res={'jean':'jean','id':5,'array':['jean','bon','5']}
print (json.dumps(res))
#print ("Hello")
```



